<?php

namespace app\controllers;


use yii\web\Controller;
use kartik\date\DatePicker;
use kartikorm\ActiveForm;
use app\models\EntryForm;


class NewsController extends Controller
{
    public function actionShow()
    {
        $this->view->title = "News";

        $heading = "Распространение нового вируса.";
        $body = "Собянин назвал минимальные сроки борьбы с коронавирусом в Москве.";
        $model = new EntryForm();


        return $this->render('show',
            [
                'heading' => $heading,
                'body' => $body,
                'model' => $model,
            ]);
    }
    public function Calendar()
    {
        echo '<br><label class="control-label">Выберите дату публикации статьи</label></br>';
        echo DatePicker::widget([
            'name' => 'dp_2',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'value' => '24.02.1996',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-M-yyyy'
            ]
        ]);
    }
}