<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->registerCssFile("@web/css/news.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);

$this->registerJsFile('@web/js/button.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);



?>

<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css%22%3E
</head>

<body class="text-center">

    <h1><?php echo $heading; ?></h1>
    <p class="lead"><?php echo $body; ?></p>
    <button type="button" class="btn btn-warning" onclick="myFunction()">Нажми</button>
<body class="text-center">

<div class="form text-center">
    <?php

    $form = ActiveForm::begin(['options'=>['style'=>'width:350px;margin:0px auto;']]);

    echo $form->field($model, 'article_heading')->label('Заголовок статьи');
    echo $form->field($model, 'article_text')->label('Текст статьи');
    ?>

    <div class="form-group" >
        <?= Html::submitButton('Выставить', ['class' => 'btn btn-primary', ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php echo $this->context->Calendar(); ?>

</body>


